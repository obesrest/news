package fr.airweb.news.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.airweb.news.R;
import fr.airweb.news.models.News;
import fr.airweb.news.views.NewsDetailsActivity;

public class NewsAdapter  extends RecyclerView.Adapter<NewsAdapter.NewsAdapterViewHolder> {

    private List<News> mNewsList;
    private Context mContext;

    @NonNull
    @Override
    public NewsAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mContext = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.news_card, viewGroup, false);

        return new NewsAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsAdapterViewHolder newsAdapterViewHolder, int i) {
        newsAdapterViewHolder.bind(mNewsList.get(i));
    }

    @Override
    public int getItemCount() {
        return mNewsList != null ? mNewsList.size() : 0;
    }

    public void setNewsList(List<News> newsList) {
        mNewsList = newsList;
        notifyDataSetChanged();
    }


    public class NewsAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.iv_image) ImageView mImageView;
        @BindView(R.id.tv_title) TextView mTitleView;
        @BindView(R.id.tv_content) TextView mContentView;

        private News mNews;


        public NewsAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);

        }

        public void bind(News news) {
            mNews = news;
            Picasso.with(mContext)
                    .load(news.getmImageUrl())
                    .into(mImageView);
            mTitleView.setText(news.getmTitle());
            mContentView.setText(news.getmContent());
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(mContext, NewsDetailsActivity.class);
            intent.putExtra(News.KEY_NEWS, mNews);
            mContext.startActivity(intent);
        }
    }
}
