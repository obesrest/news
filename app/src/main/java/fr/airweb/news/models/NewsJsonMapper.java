package fr.airweb.news.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class NewsJsonMapper {

    private static final String JSON_PROPERTY_NEWS_NID = "nid";
    private static final String JSON_PROPERTY_NEWS_TYPE = "type";
    private static final String JSON_PROPERTY_NEWS_DATE = "date";
    private static final String JSON_PROPERTY_NEWS_TITLE = "title";
    private static final String JSON_PROPERTY_NEWS_PICTURE = "picture";
    private static final String JSON_PROPERTY_NEWS_CONTENT = "content";
    private static final String JSON_PROPERTY_NEWS_DATEFORMATED = "dateformated";

    private static final String TYPE_NEWS = "news";

    public News createNews(JSONObject jsonObject) throws MalformedJsonNews {
        try {
            String dateString = jsonObject.getString(JSON_PROPERTY_NEWS_DATE);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date date;
            try {
                date = format.parse(dateString);
            } catch (ParseException e) {
                e.printStackTrace();
                throw new MalformedJsonNews(jsonObject);
            }
            String type = jsonObject.getString(JSON_PROPERTY_NEWS_TYPE);
            switch (type) {
                case TYPE_NEWS:
                    return new NewsNews(jsonObject.getInt(JSON_PROPERTY_NEWS_NID),
                            jsonObject.getString(JSON_PROPERTY_NEWS_TYPE),
                            date,
                            jsonObject.getString(JSON_PROPERTY_NEWS_TITLE),
                            jsonObject.getString(JSON_PROPERTY_NEWS_PICTURE),
                            jsonObject.getString(JSON_PROPERTY_NEWS_CONTENT),
                            jsonObject.getString(JSON_PROPERTY_NEWS_DATEFORMATED));
                    default:
                        throw new MalformedJsonNews("Unknown news type", jsonObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            throw new MalformedJsonNews(jsonObject);
        }
    }
}
