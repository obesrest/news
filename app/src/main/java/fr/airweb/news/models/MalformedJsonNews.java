package fr.airweb.news.models;

import org.json.JSONException;
import org.json.JSONObject;

public class MalformedJsonNews extends JSONException {

    public MalformedJsonNews(JSONObject jsonObject) {
        this("Unable to parse Json news Object: ", jsonObject);
    }

    public MalformedJsonNews(String message, JSONObject jsonObject) {
        super(message + jsonObject.toString());
    }
}
