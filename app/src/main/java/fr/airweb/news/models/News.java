package fr.airweb.news.models;

import java.io.Serializable;
import java.util.Date;

public class News implements Serializable {

    public static final String KEY_NEWS = "news";

    private int mNId;
    private String mType;
    private Date mDate;
    private String mTitle;
    private String mImageUrl;
    private String mContent;
    private String mDateFormated;


    public News(int nId, String type, Date date, String title, String url, String content, String dateFormated) {
        mNId = nId;
        mType = type;
        mDate = date;
        mTitle = title;
        mImageUrl = url;
        mContent = content;
        mDateFormated = dateFormated;
    }

    public int getmNId() {
        return mNId;
    }

    public String getmType() {
        return mType;
    }

    public Date getmDate() {
        return mDate;
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmImageUrl() {
        return mImageUrl;
    }

    public String getmContent() {
        return mContent;
    }

    public String getmDateFormated() {
        return mDateFormated;
    }


}
