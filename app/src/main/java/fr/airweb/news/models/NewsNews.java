package fr.airweb.news.models;

import java.util.Date;

public class NewsNews extends News {
    public NewsNews(int nId, String type, Date date, String title, String url, String content, String dateFormated) {
        super(nId, type, date, title, url, content, dateFormated);
    }
}
