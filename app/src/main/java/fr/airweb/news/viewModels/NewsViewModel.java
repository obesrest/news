package fr.airweb.news.viewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import fr.airweb.news.interactors.NewsListInteractor;
import fr.airweb.news.models.News;

public class NewsViewModel extends ViewModel {

    private MutableLiveData<List<News>> newsList;

    public void init(NewsListInteractor newsListInteractor) {
        newsList = newsListInteractor.getNewsList();
    }

    public LiveData<List<News>> getNewsList() {
        return newsList;
    }
}
