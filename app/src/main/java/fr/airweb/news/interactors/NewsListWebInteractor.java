package fr.airweb.news.interactors;

import android.app.Activity;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.android.volley.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import fr.airweb.news.models.News;
import fr.airweb.news.models.NewsJsonMapper;
import fr.airweb.news.models.NewsNews;
import fr.airweb.news.repositories.WebJSONRepository;

public class NewsListWebInteractor implements NewsListInteractor {

    public static String JSON_PROPERTY_NEWS = "news";

    private WebJSONRepository repo;

    public NewsListWebInteractor(Activity activity) {
        repo = new WebJSONRepository(activity);
    }

    @Override
    public MutableLiveData<List<News>> getNewsList() {

        final MutableLiveData<List<News>> newsList = new MutableLiveData<>();

        repo.getJSONObject(null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                newsList.setValue(getNewsListFromJSONObject(response));
            }
        });
        return newsList;
    }

    private List<News> getNewsListFromJSONObject(JSONObject jsonObject) {
        List<News> newsList = new ArrayList<>();
        try {
            JSONArray newsArray = jsonObject.getJSONArray(JSON_PROPERTY_NEWS);
            for (int i = 0 ; i < newsArray.length() ; i++) {
                try {
                    JSONObject newsJsonObjectItem = newsArray.getJSONObject(i);
                    News newsItem = new NewsJsonMapper().createNews(newsJsonObjectItem);
                    if (newsItem instanceof NewsNews) {
                        newsList.add(newsItem);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return newsList;
    }
}
