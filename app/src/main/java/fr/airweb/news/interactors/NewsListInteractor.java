package fr.airweb.news.interactors;

import android.arch.lifecycle.MutableLiveData;

import java.util.List;

import fr.airweb.news.models.News;

public interface NewsListInteractor {

    MutableLiveData<List<News>> getNewsList();
}
