package fr.airweb.news.repositories;

import android.app.Activity;
import android.support.annotation.Nullable;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

public class WebJSONRepository {

    private static final String TAG = WebJSONRepository.class.getSimpleName();
    public static final String NEWS_LIST_URL = "https://airweb-demo.airweb.fr/psg/psg.json";
    private Activity mActivity;

    public WebJSONRepository(Activity activity) {
        mActivity = activity;
    }

    public void getJSONArray(@Nullable String urlData, Response.Listener<JSONArray> listener) {

            RequestQueue queue = Volley.newRequestQueue(mActivity);

            String url = urlData == null ? NEWS_LIST_URL : NEWS_LIST_URL + urlData;
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                    listener,
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    });

            queue.add(jsonArrayRequest);

    }

    public void getJSONObject(@Nullable String urlData, Response.Listener<JSONObject> listener) {

        RequestQueue queue = Volley.newRequestQueue(mActivity);

        String url = urlData == null ? NEWS_LIST_URL : NEWS_LIST_URL + urlData;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                listener,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });

        queue.add(jsonObjectRequest);
    }

}
