package fr.airweb.news.views;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.airweb.news.R;
import fr.airweb.news.adapters.NewsAdapter;
import fr.airweb.news.interactors.NewsListInteractor;
import fr.airweb.news.interactors.NewsListWebInteractor;
import fr.airweb.news.models.News;
import fr.airweb.news.viewModels.NewsViewModel;

public class NewsActivity extends AppCompatActivity {

    @BindView(R.id.recyclerview_news)
    RecyclerView mRecyclerView;
    @BindView(R.id.pb_loading_indicator)
    ProgressBar mLoadingIndicator;
    @BindView(R.id.tv_message_empty_book_list)
    TextView mEmptyListMessage;

    private NewsAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        ButterKnife.bind(this);

        NewsListInteractor mNewsListInteractor = new NewsListWebInteractor(this);
        mAdapter = new NewsAdapter();

        NewsViewModel mViewModel = ViewModelProviders.of(this).get(NewsViewModel.class);
        mViewModel.init(mNewsListInteractor);
        mViewModel.getNewsList().observe(this, new Observer<List<News>>() {
            @Override
            public void onChanged(@Nullable List<News> news) {
                mAdapter.setNewsList(news);
                if (news == null || news.isEmpty()) {
                    showBookListEmptyMessage();
                } else {
                    showMainView();
                }
            }
        });

        mRecyclerView.setAdapter(mAdapter);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);


        showLoadingIndicator();
    }


    private void showLoadingIndicator() {
        mRecyclerView.setVisibility(View.GONE);
        mLoadingIndicator.setVisibility(View.VISIBLE);
        mEmptyListMessage.setVisibility(View.GONE);
    }

    private void showBookListEmptyMessage() {
        mRecyclerView.setVisibility(View.GONE);
        mLoadingIndicator.setVisibility(View.GONE);
        mEmptyListMessage.setVisibility(View.VISIBLE);
    }

    private void showMainView() {
        mRecyclerView.setVisibility(View.VISIBLE);
        mLoadingIndicator.setVisibility(View.GONE);
        mEmptyListMessage.setVisibility(View.GONE);
    }
}
