package fr.airweb.news.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.airweb.news.R;
import fr.airweb.news.models.News;

public class NewsDetailsActivity extends AppCompatActivity {

    @BindView(R.id.iv_image)
    ImageView mImageView;
    @BindView(R.id.tv_title)
    TextView mTitleView;
    @BindView(R.id.tv_content) TextView mContentView;

    News mNews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(News.KEY_NEWS)) {
            mNews = (News) intent.getSerializableExtra(News.KEY_NEWS);
            Picasso.with(this)
                    .load(mNews.getmImageUrl())
                    .into(mImageView);
            mTitleView.setText(mNews.getmTitle());
            mContentView.setText(mNews.getmContent());

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        for (int i = 0; i < menu.size() ; i++) {
            if (menu.getItem(i).getItemId() != R.id.action_share) {
                menu.getItem(i).setVisible(false);
            }
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_share) {
            Toast.makeText(this, "Je partage !", Toast.LENGTH_LONG).show();
        }
        return super.onOptionsItemSelected(item);
    }


}
